# include <iostream>
# include "Sorter.hpp"

// Divides unsorted list in three lists(for R,G,B) and then combines it
// in one with respect to rule
std::list<Object> Sorter::sort_list(std::list<Object> l, std::string rule) {
	std::list<Object> characterR, characterB, characterG, outList;
	for (auto it = l.begin(); it != l.end(); ++it) {
		if ( (*it)._color == 'R' ) { characterR.push_back(*it); }
		if ( (*it)._color == 'B') { characterB.push_back(*it); }
		if ( (*it)._color == 'G') { characterG.push_back(*it); }
	}
	for (auto it = rule.begin(); it != rule.end(); ++it) {
		if (*it == 'R') { outList.splice(outList.end(), characterR); }
		if (*it == 'B') { outList.splice(outList.end(), characterB); }
		if (*it == 'G') { outList.splice(outList.end(), characterG); }
	}
	/*
	std::cout << "Unsorted: " << std::endl;
	for (auto const& i : l) {
		std::cout << i._color;
	}
	std::cout << std::endl;
	std::cout << " Sorted: " << std::endl;
	for (auto const& i : outList) {
		std::cout << i._color;
	}
	std::cout << std::endl;
	*/
	return outList;
}
