# include "Manager.hpp"
# include <iostream>
# include <string>

void Manager::start() {
	_running = true;
	while (_running) {
		std::string command = fetch_command();
		execute_command(command);
	}
	if (_producerThrPtr) {
		_producerThrPtr->join();
	}
	if (_sorterThrPtr) {
		_sorterThrPtr->join();
	}
}

std::string Manager::fetch_command() {
	std::string command;
	std::cout << "Please enter the command to proceed:" << std::endl;
	std::getline(std::cin, command);
	return command;
}
// available commands. TODO :
// add "show list of commands"
// checks for every input for set_ methods (e.g. std::transform(_rule.begin(), _rule.end(), _rule.begin(), ::toupper);)
void Manager::execute_command(std::string c) {
	if (c == "set rule") {
		set_rule();
	}
	else if (c == "start producer") {
		start_producer();
	}
	else if (c == "stop producer") {
		stop_producer();
	}
	else if (c == "start sorter") {
		start_sorter();
	}
	else if (c == "stop sorter") {
		stop_sorter();
	}
	else if (c == "enable log") {
		enable_log();
	}
	else if (c == "disable log") {
		disable_log();
	}
	else if (c == "set list size") {
		set_list_size();
	}
	else if (c == "set nElements") {
		set_nelements();
	}
	else if (c == "exit") {
		exit();
	}
	else {
		std::cout << "Command not found..." << std::endl;
	}
}
void Manager::set_rule() {
	if (_sorterThrPtr != nullptr) {
		std::cout << "Please, stop sorter first..." << std::endl;
	}
	else {
		std::cout << "Please set rule for sorting:" << std::endl;
		std::getline(std::cin, _rule);
	}
}
void Manager::set_nelements() {
	std::cout << "Please enter maximum number of elements in queue:" << std::endl;
	std::string s;
	std::getline(std::cin, s);
	_nElements = std::stoi(s);
}
void Manager::set_list_size() {
	std::cout << "Please set string size to be generated:" << std::endl;
	std::string s;
	std::getline(std::cin, s);
	_listSize = std::stoi(s);
}
void Manager::exit() {
	stop_producer();
	stop_sorter();
	_running = false;
	if (_sorterStreamPtr != nullptr) {
		_sorterStreamPtr->close();
	}
}
void Manager::start_producer() {
	if (_producerThrPtr != nullptr) {
		std::cout << "Producer already started" << std::endl;
	}
	else {
		this->_stopProducer = false;
		_producerThrPtr = new std::thread(&Manager::add_to_queue, this);
	}
}

void Manager::add_to_queue() {
	std::cout << "Starting producing" << std::endl;
	while (this->_stopProducer == false) {
		if (this->_queue.size() < this->_nElements) {
			std::list<Object> l = _producerPtr->produce_list(this->_listSize);
			this->_queue.push(l);
			//std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
	}
	std::cout << "Stopping producing" << std::endl;
}
void Manager::stop_producer() {
	if (this->_producerThrPtr == nullptr) {
		std::cout << "Producer are not running" << std::endl;
	}
	else {
		this->_stopProducer = true;
		_producerThrPtr->join();
		delete this->_producerThrPtr;
		this->_producerThrPtr = nullptr;
	}
}
void Manager::start_sorter() {
	if (_sorterThrPtr != nullptr) {
		std::cout << "Sorter already started" << std::endl;
	}
	else {
		this->_stopSorter = false;
		_sorterThrPtr = new std::thread(&Manager::sort_from_queue, this);
	}
}

void Manager::sort_from_queue() {
	std::cout << "Starting sorting" << std::endl;
	while (this->_stopSorter == false) {
		if (!_queue.empty()) {
			std::list<Object> l;
			l = _queue.front();
			_queue.pop();
			std::list<Object> sortedList = _sorterPtr->sort_list(l, this->_rule);
			if (_enableLog) {
				this->write_to_log(l, sortedList);
			}
			//std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
	}
	std::cout << "Stopping sorting" << std::endl;
}
void Manager::stop_sorter() {
	if (this->_sorterThrPtr == nullptr) {
		std::cout << "Sorter are not running" << std::endl;
	}
	else {
		this->_stopSorter = true;
		_sorterThrPtr->join();
		delete this->_sorterThrPtr;
		this->_sorterThrPtr = nullptr;
	}
}

void Manager::enable_log() {
	if (_sorterThrPtr != nullptr) {
		std::cout << "Please, stop sorter first..." << std::endl;
	}
	else {
		if (_enableLog == true) {
			std::cout << "Log is enbled already..." << std::endl;
		}
		else {
			if (_sorterStreamPtr == nullptr) {
				_sorterStreamPtr = new std::ofstream("SorterLog.txt");
				_enableLog = true;
			}
			else {
				_sorterStreamPtr = new std::ofstream("SorterLog.txt", std::ios_base::app);
				_enableLog = true;
			}
		}
	}
}
void Manager::disable_log() {
	if (_sorterThrPtr != nullptr) {
		std::cout << "Please, stop sorter first..." << std::endl;
	}
	else {
		if (_enableLog == false) {
			std::cout << "Log is disabled already..." << std::endl;
		}
		else {
			if (_sorterStreamPtr != nullptr) {
				_enableLog = false;
				_sorterStreamPtr->close();
			}
		}
	}
}
void Manager::write_to_log(std::list<Object> unsortedList, std::list<Object> sortedList) {
	*_sorterStreamPtr << "Unsorted: " << std::endl;
	for (auto const& i : unsortedList) {
		*_sorterStreamPtr << i._color;
	}
	*_sorterStreamPtr << std::endl;
	*_sorterStreamPtr << "Sorted:" << std::endl;
	for (auto const& i : sortedList) {
		*_sorterStreamPtr << i._color;
	}
	*_sorterStreamPtr << std::endl;
}