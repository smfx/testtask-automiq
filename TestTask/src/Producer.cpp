# include <iostream>

# include "Producer.hpp"

std::list<Object> Producer::produce_list(size_t sizeOfList) {
	std::list<Object> l;
	int randomNumber;
	for (size_t i = 0; i < sizeOfList; ++i) {
		Object o;
		randomNumber = (rand() % _nPossibleVariants);
		if (randomNumber == 0) { o._color = 'R'; }
		else if (randomNumber == 1) { o._color = 'B'; }
		else { o._color = 'G'; }
		l.push_back(o);
	}
	return l;
}
