// Let Catch provide main():
# define CATCH_CONFIG_MAIN

# include "catch.hpp"
# include "Producer.hpp"
# include "Sorter.hpp"
# include "Manager.hpp"
# include "Object.hpp"


TEST_CASE("Producer tests", "[single-file]") {
    Producer * p = new Producer();
    std::list<Object> list0 = p->produce_list(0);
    std::list<Object> list5 = p->produce_list(5);
    std::list<Object> list42 = p->produce_list(42);
    REQUIRE(list0.size() == 0);
    REQUIRE(list0.empty() == true);
    REQUIRE(list5.size() == 5);
    REQUIRE(list42.size() == 42);
}
std::string list_to_string(std::list<Object> list) {
    std::string string;
    for (auto it = list.begin(); it != list.end(); ++it) {
        string += (*it)._color;
    }
    return string;
}
TEST_CASE("Sorter tests", "[single-file]") {
    Sorter* s = new Sorter();
    Object R, G, B;
    R._color = 'R';
    G._color = 'G';
    B._color = 'B';
    std::list<Object> unsortedList, sortedList;
    std::string result;
    sortedList = s->sort_list(unsortedList, "RGB");
    REQUIRE(sortedList.empty() == true);
    unsortedList.push_back(B);  // B
    sortedList = s->sort_list(unsortedList, "RGB");
    result = list_to_string(sortedList);
    REQUIRE(result == "B");
    unsortedList.push_back(B);  // BB
    unsortedList.push_back(R);  // BBR
    unsortedList.push_back(G);  // BBRG
    unsortedList.push_back(G);  // BBRGG
    unsortedList.push_back(R);  // BBRGGR
    sortedList = s->sort_list(unsortedList, "RGB");  // BBRGGR -> RRGGBB
    result = list_to_string(sortedList);
    REQUIRE(result == "RRGGBB");
    sortedList = s->sort_list(unsortedList, "BGR");  // BBRGGR -> BBGGRR
    result = list_to_string(sortedList);
    REQUIRE(result == "BBGGRR");
    unsortedList.clear();
    sortedList = s->sort_list(unsortedList, "BGR");  // should be empty
    result = list_to_string(sortedList);
    REQUIRE(result == "");
}
TEST_CASE("Manager tests", "[single-file]") {
    GuardedQueue queue;
    Producer* producerPtr = new Producer();
    Sorter* sorterPtr = new Sorter();
    Manager* managerPtr = new Manager(queue, producerPtr, sorterPtr);
    REQUIRE( managerPtr->get_rule() == "RGB");
    managerPtr->set_rule("BRG");
    REQUIRE(managerPtr->get_rule() == "BRG");
}