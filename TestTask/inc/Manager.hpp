# pragma once

# include <fstream>
# include <thread>  // could be used <boost/thread.hpp> instead
# include "GuardedQueue.hpp"
# include "Producer.hpp"
# include "Sorter.hpp"

class Manager {
	public:
		Manager(GuardedQueue &q, Producer *p, Sorter *s) : _queue(q), _producerPtr(p), _producerThrPtr(nullptr),
			_sorterPtr(s), _sorterThrPtr(nullptr), _running(false), _stopProducer(false), _stopSorter(false), _enableLog(false),
			_rule("RGB"), _listSize(5), _nElements(5), _sorterStreamPtr(nullptr) {};
		~Manager() {};
		// This method starts Manager. After that users are able to execute commands
		void start();
		// Fetching commands
		std::string fetch_command();
		// Execute fetched command
		void execute_command(std::string);
		// Start producer. Producer starts to write sets (std::list<Object>) to _queue
		void start_producer();
		void stop_producer();
		// Start sorter. Sorter starts to sort sets from _queue
		void start_sorter();
		void stop_sorter();
		// Allows to set maximum elements in the generated lists (_listSize)
		void set_list_size();
		// Allows to set maximum elements in queue (_nElements). If producer produces elements to fast
		// (or working without sorter, which means elements only are added to _queue but never taken from it)
		// _queue size could grow dramatically. So, if queue's size reaches _nElements producer would stop produce
		// new elements
		void set_nelements();
		// Stopping Producer, Sorter and exit
		void exit();
		// Called in start_producer() method
		void add_to_queue();
		// Called in start_sorter() method
		void sort_from_queue();
		// Enable/disable output to log file. Should be expanded by adding possibilities to rename file's name,
		// change streams and etc. Also, Manager does not control file size, which is dangerous and should be
		// changed
		void enable_log();
		void disable_log();
		void write_to_log(std::list<Object>, std::list<Object>);
		// Rule for sorting. For example, if Producer produced set of objects with following color order:
		// BBRGRG and rule is RGB, Sorter would return set of objects with RRGGBB color order.
		std::string get_rule() { return _rule; }
		void set_rule();
		void set_rule(std::string s) { _rule = s; }
	protected:
		GuardedQueue& _queue;
		Producer* _producerPtr;
		std::thread * _producerThrPtr;
		Sorter* _sorterPtr;
		std::thread* _sorterThrPtr;
		bool _running;
		bool _stopProducer;
		bool _stopSorter;
		bool _enableLog;
		std::string _rule;
		size_t _listSize;
		size_t _nElements;
		std::ofstream* _sorterStreamPtr;
};