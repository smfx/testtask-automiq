# pragma once

# include "GuardedQueue.hpp"

class Producer {
	public:
		// initialize seed for rand()
		Producer() { srand((unsigned)time(0)); };
		~Producer() {};
		// produce list of elements with type Object
		std::list<Object> produce_list(size_t);
	private:
		// For now, we only produce three type of color: R,G,B
		int _nPossibleVariants = 3;
};