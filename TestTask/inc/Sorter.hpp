# pragma once

# include "GuardedQueue.hpp"

class Sorter {
	public:
		Sorter() {};
		~Sorter() {};
		// Take unsorted list and sorts it with respect to std::string rule.
		// For example, if given list of objects has following color order :
		// BBRGRG and rule is RGB, this method would return list of objects with RRGGBB color order.
		std::list<Object> sort_list(std::list<Object> l, std::string);
};