# pragma once

# include <queue>
# include <mutex>  // could be used <boost/thread/mutex.hpp>
# include <list>
# include "Object.hpp"

class GuardedQueue : public std::queue<std::list<Object>> {
		std::mutex accessGuard;
		std::queue<std::list<Object>> _q;

	public:
		bool empty() {
			bool isEmpty;
			accessGuard.lock();
			isEmpty = _q.empty();
			accessGuard.unlock();
			return isEmpty;
		}
		size_t size() {
			size_t size;
			accessGuard.lock();
			size = _q.size();
			accessGuard.unlock();
			return size;
		}
		void push(std::list<Object> s) {
			accessGuard.lock();
			_q.push(s);
			accessGuard.unlock();
		}
		std::list<Object> front() {
			std::list<Object> l;
			accessGuard.lock();
			l = _q.front();
			accessGuard.unlock();
			return l;
		};
		void pop() {
			accessGuard.lock();
			_q.pop();
			accessGuard.unlock();
		}
};