﻿# include <iostream>
# include <fstream>

# include "GuardedQueue.hpp"
# include "Producer.hpp"
# include "Sorter.hpp"
# include "Manager.hpp"

int main(int argc, char** argv)
{
	GuardedQueue queue;
	Producer * producerPtr = new Producer();
	Sorter * sorterPtr = new Sorter();
	Manager* managerPtr = new Manager(queue, producerPtr, sorterPtr);

	managerPtr->start();

	return 0;
}
